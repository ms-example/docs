export default {
    // site-level options
    title: 'Microservices Example',
    description: 'Project contains examples of GoLang microservices.',
    ignoreDeadLinks: true,
    srcExclude: ['public'],
  
    themeConfig: {
        search: {
            provider: 'local'
        },
        nav: [
            { text: 'Home', link: '/' },
            { text: 'GitLab', link: 'https://gitlab.com/ms-example/docs' },
        ],
        sidebar: [
            {
              text: 'Documentation',
              items: [
                { text: 'About', link: '/about' },
              ]
            },
            {
              text: 'Protobuf',
              items: [
                { text: 'userspb', link: 'protobuf/userspb/userspb.md' },
                { text: 'notifierpb', link: 'protobuf/notifierpb/notifierpb.md' },
              ]
            },
            {
              text: 'Services',
              items: [
                { text: 'users-service', link: '/users-service/README.html' },
              ]
            }
          ]
    },
  }
